# Amazon PuppeteerCrawler tutorial

Amazon Gitlab tutorial made by Hoang Anh Tuan.

## Table of contents

<!-- toc start -->
- Introduction
- Input
- Output
- Author's notes
<!-- toc end -->


## Introduction
Actor finds products' info about products by keyword submitted by user found on https://www.amazon.com/.
Actor is optimized for going through Amazon Website via residential proxy located in Czech Republic.

## Input
Input is located in `.\apify_storage\key_value_stores\default` named `INPUT.JSON`.

Input file should look like this
```
{
"keyword": "iphone"
}
```
## Output
Output is located in `.\apify_storage\datasets\default` and each product is separated by JSON file.

Each available product is described with
- `keyword` - with which was the product found
- `title` - title shown on Amazon website
- `url` - from where info was extracted from
- `description` - description of the product
> Note: Description is found by `#productDescription p`, which some products doesn't have
- `sellerName` - name of the seller
- `price` - price of the product sold by `sellerName`
- `shipping` - shipping cost

For `keyword` `iphone`, one of the JSON files should look like this.
```
{
  "result": [
    {
      "keyword": "iphone",
      "title": "(Refurbished) Apple iPhone 11, US Version, 64GB, Black - Unlocked",
      "url": "https://www.amazon.com/dp/B07ZPKN6YR",
      "description": "Just the right amount of everything. A new dual‑lens rear system captures more of what you see and love. The fastest chip ever in a smartphone and all‑day battery life let you do more and charge less. And the highest‑quality video in a smartphone, so your memories look better than ever.",
      "sellerName": "BuySPRY",
      "price": "$514.00",
      "shipping": "$43.63"
    },
    {
      "keyword": "iphone",
      "title": "(Refurbished) Apple iPhone 11, US Version, 64GB, Black - Unlocked",
      "url": "https://www.amazon.com/dp/B07ZPKN6YR",
      "description": "Just the right amount of everything. A new dual‑lens rear system captures more of what you see and love. The fastest chip ever in a smartphone and all‑day battery life let you do more and charge less. And the highest‑quality video in a smartphone, so your memories look better than ever.",
      "sellerName": "Wcell",
      "price": "$515.56",
      "shipping": "$43.63"
    }
  ]
}
```

## Author's notes
I struggled a lot. A LOT. It is because I'm new not only with Apify SDK (its DOCs, Guide and Help helped me a lot), but also with JS and CSS, so basically I had to learn everything. Let me list my struggles:
- Amazon web inconsistency - for some reason, some Amazon web products have different source codes, so I basically have to try to catch them all, because it uses different class names, IDs, structure and so on. For example I caught that on some product website title is located in div element with class ID `productTitle` but on some other products it is in div element with class name `qa-title-tex`.
- Amazon international web incosistency - US and CZ Amazon product websites are different :) For example, it is almost impossible ( for me with my current skills) to retrieve US website shipment cost, while on Czech website it is shown on plain sight.
- Proxies - (btw Thank You, Lukas, for proxy seminar, it helped me a lot to understand what's going on) it pretty much ties up with previous two points, if Amazon website is searched through US, the page looks/works very differently than from CZ and I wasn't aware of that at first.
- my knowledge of technologies - going through guide i felt like I understand everything, but with this task, it opened my eyes about what I have to learn. While coding, I read a lot of articles to help me understand some methods and logic of technologies and I made it work. I'm pretty sure not ideally, so I would like to kindly ask Lukas to show me the way to do everything properly.
- debugging - I hope there is a better way to debug, then simply trying actor over and over again. I wasted like $30 on my apify residential proxy data transfer and I expect to waste more to properly finish the project.

Although I struggled a lot, I really enjoy it and I unexpectedly worked all day even through minor sickness and light headache
