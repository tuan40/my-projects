const Apify = require('apify');

const { handleStart, handleList, handleDetail } = require('./src/routes');

const { utils: { log } } = Apify;

/*
const client = new ApifyClient({
    token: Apify.getEnv().token,
});

await Apify.addWebhook({
    eventTypes: ['ACTOR.RUN.SUCCEEDED'],
    requestUrl: `https://api.apify.com/v2/acts/ACTOR_NAME_OR_ID/runs?token=${Apify.getEnv().token}`,
});

*/

Apify.main(async () => {
    // get input and add to requestQueue
    const input = await Apify.getInput();
    const keyword = input['keyword'];
    const dataObj = {keyword: keyword};
    const requestQueue = await Apify.openRequestQueue();
    await requestQueue.addRequest({
        url: `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${keyword}`,
        userData: {
            label: "START",
            data: dataObj
        }
    });

    // configure proxy
    const proxyConfiguration = await Apify.createProxyConfiguration({
        groups: ['RESIDENTIAL'],
        countryCode: 'CZ',
    });

    const crawler = new Apify.PuppeteerCrawler({
        requestQueue,
        proxyConfiguration,
        launchContext: {
            useChrome: true,
            stealth: true,
            launchOptions: {
                headless: false, // TODO: not working for headless
            },
        },
        handlePageFunction: async (context) => {
            const { url, userData: { label } } = context.request;
            switch (label) {
                case 'LIST': // list options
                    return handleList(context);
                case 'DETAIL': // detail of the product
                    return handleDetail(context, requestQueue);
                default: // start url
                    return handleStart(context, requestQueue);
            }
        },
    });

    log.info('Starting the crawl.');
    await crawler.run();
    log.info('Crawl finished.');

    log.info('Webhook called.');
    const selectCheapestActor = '8fOKOWqX60AIyJdQv';
    await Apify.addWebhook({
        eventTypes: ['ACTOR.RUN.SUCCEEDED'],
        requestUrl: `https://api.apify.com/v2/acts/${selectCheapestActor}/runs?token=${Apify.getEnv().token}`
    });
});
