const Apify = require('apify');

const { utils: { log } } = Apify;

// starting url searching with input keyword
// adds all the item on the first page to the requestQueue
exports.handleStart = async ({ request, page }, requestQueue) => {
    log.info(`[START]: URL - '${page.url()}' opened.`);

    const asins = await page.$$eval('.s-result-item.s-asin', $item => {
        const items = [];
        $item.forEach($item => {
            items.push($item.getAttribute("data-asin"));
        });

        return items;
    });
    
    log.info(`[START]: Products found: ${asins.length}`);

    for (const asin of asins) {
        const itemURL = `https://www.amazon.com/dp/${asin}`
        await requestQueue.addRequest({
            url: itemURL,
            userData: {
                label: "DETAIL",
                data: request.userData.data
            }
        });
    }

    log.info('[START]: Products queued.');
}

// finding list of available options of a certain product
// pushing all the scraped data to output
exports.handleList = async ({ request, page }) => {
    log.info(`[DETAIL]: URL - '${page.url()}' opened.`);

    // wait for js to load  
    /*
    debug screenshot for headless chrome
    await page.waitFor(10000);
    await page.screenshot({path: 'example.png'});
     */
    log.info(`[LIST]: Waiting for the list bar to load.`);
    await page.waitForSelector('#aod-message-component');

    // find if there is any offer
    const offerCount = await page.evaluate(() => {
        const offerEl = document.querySelector('#aod-total-offer-count');
        if (!offerEl) {
            return 0;
        }
        return offerEl.getAttribute('value').textContent;
    })

    if (offerCount <= 0) {
        log.info('[LIST]: Item has no available offers.');
        return;
    }

    log.info(`[LIST]: Getting offers info.`);
    // find info to save
    const offers = await page.$$eval('#aod-offer', $item => {
        const items = [];
        $item.forEach($item => {
            const shipping = $item.querySelector('span span.a-color-secondary.a-size-base').textContent;
            const shippingPrice = shipping.replace(/[^\$\d.-]/g, '');
            const seller = $item.querySelector('.a-size-small.a-link-normal').textContent.trim();
            const price = $item.querySelector('.a-offscreen').textContent.trim();

            const sellInfoObj = {'sellerName': seller, 'price': price, 'shipping': shippingPrice};

            items.push(sellInfoObj);
        });
        return items;
    });

    // add previous data to new data to push
    for(const offer of offers){
        const dataObj = Object.assign({}, request.userData.data, offer);
        await Apify.pushData(dataObj);
    }

    log.info('[LIST]: Offers pushed.');
}

// finds data on the product detail page
// adds product to the requestQueue to find list options if available 
exports.handleDetail = async ({ request, page }, requestQueue) => {
    const url = page.url();
    log.info(`[DETAIL]: URL - '${url}' opened.`);

    log.info(`[DETAIL]: Getting item info.`);
    // find title
    const title = await page.evaluate(() => {
        let productEl = document.getElementById('productTitle');
        if (productEl)
            return productEl.textContent.trim();
        else {
            productEl = document.getElementsByClassName('qa-title-text')[0];
            if (productEl)
                return productEl.textContent.trim();
            else {
                // TODO: how to handle unknown title? 
                console.log("ERROR");
                console.log("Couldn't find title");
                return "not found";
            }
        }
    })

    // check if product has price for availability
    // does not necessary mean it has an offer for certain country
    const available = await page.evaluate(() => {
        let priceEl = document.getElementById('priceblock_ourprice');
        if (priceEl)
            return true;
        else {
            priceEl = document.getElementById('qa-price-block-our-price');
            if (priceEl)
                return true;
            // TODO: how to handle unknown price?
        }

        return false;
    })

    // get description, doesn't have to be there
    const description = await page.evaluate(() => {
        const descriptionEl = document.querySelector('#productDescription p');

        if (descriptionEl)
            return descriptionEl.textContent.trim();
        return "";
    })

    // package scraped data for next url
    const itemDetail = { title, url, description };
    const dataObj = Object.assign({}, request.userData.data, itemDetail);
    
    // add available products for list options
    if (available) {
        log.info(`[DETAIL]: Item queued.`);
        const asin = url.substring(url.lastIndexOf('/') + 1, url.length);
        const itemURL = `https://www.amazon.com/gp/offer-listing/${asin}`
        requestQueue.addRequest({
            url: itemURL,
            userData: {
                label: "LIST",
                data: dataObj
            }
        });
    }
}